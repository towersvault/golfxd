package com.towersvault.golfxd.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.towersvault.golfxd.MainGolfxD;
import com.towersvault.golfxd.util.Constants;
import com.towersvault.golfxd.util.AspectRatioHelper;

public class DesktopLauncher
{
	public static void main (String[] arg)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = Constants.INSTANCE.getGameName() + " " + Constants.INSTANCE.getVersion();

		int fullscreenWidth = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		int fullscreenHeight = LwjglApplicationConfiguration.getDesktopDisplayMode().height;

		if(Constants.INSTANCE.getReleaseMode())
		{
			config.width = fullscreenWidth;
			config.height = fullscreenHeight;

			AspectRatioHelper.inst.baseRatio = (float)fullscreenWidth / (float)fullscreenHeight;
		}
		else
		{
			fullscreenWidth = 1280;
			fullscreenHeight = 720;

			config.width = fullscreenWidth;
			config.height = fullscreenHeight;
		}

		AspectRatioHelper.inst.baseRatio = (float)fullscreenWidth / (float)fullscreenHeight;

		config.resizable = false;
		config.fullscreen = Constants.INSTANCE.getReleaseMode();

		new LwjglApplication(new MainGolfxD(), config);
	}
}
