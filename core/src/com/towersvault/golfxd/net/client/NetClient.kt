package com.towersvault.golfxd.net.client

import com.badlogic.gdx.utils.Array
import com.esotericsoftware.kryonet.Client
import com.towersvault.golfxd.game.GolfballHandler
import com.towersvault.golfxd.net.host.GameHandler
import com.towersvault.golfxd.net.*
import com.towersvault.golfxd.net.host.NetGolfball
import com.towersvault.golfxd.screens.Scene2DHelper
import com.towersvault.golfxd.util.Constants
import com.towersvault.golfxd.util.Log

object NetClient
{
	var client = Client()
	
	val tcp = 21189
	val udp = 28189
	
	var netData = NetData()
	var netHello = NetHello()
	
	var username = ""
	var uuid = ""
	
	fun init()
	{
		Log.post("Initializing client.")
		
		client.start()
		
		var listener = ClientListener()
		
		client.addListener(listener)
		
		var k = client.kryo
		k.register(NetData::class.java)
		k.register(NetHello::class.java)
		k.register(NetDisconnect::class.java)
		k.register(NetMessage::class.java)
		k.register(NetStart::class.java)
		k.register(NetHole::class.java)
		k.register(NetUpdate::class.java)
		k.register(NetGolfball::class.java)
	
		Log.post("Initialized client.")
	}
	
	fun connect(): Boolean
	{
		Scene2DHelper.donePointing = false
		
		Scene2DHelper.setConnectingText("Connecting...")
		
		var splitIp = Constants.ipAddress.split(".")
		var ipAddress = "${splitIp[0]}.${splitIp[1]}.${splitIp[2]}.${Scene2DHelper.lobbyId}"
		
		Log.post("Connecting to host $ipAddress")
		
		//return false
		
		try
		{
			client.connect(5000, ipAddress, tcp, udp)
			Log.post("Connected to host $ipAddress. Initializing handshake.")
			
			netHello.uuid = username
			netHello.username = username
			client.sendTCP(netHello)
			
			Log.post("Host connection handshake initialized.")
			
			Scene2DHelper.setConnectingText("Connected!")
			
			return true
		}
		catch(e: Exception)
		{
			Log.post("Failed to connect to host $ipAddress", Log.Level.ERR)
			
			Scene2DHelper.setConnectingText("Failed to connect.")
			
			return false
		}
	}
	
	fun connectLocalhost(): Boolean
	{
		Log.post("Connecting to localhost.")
		
		try
		{
			client.connect(5000, Constants.ipAddress, tcp, udp)
			Log.post("Connected to localhost. Initializing handshake.")
			
			netHello.uuid = username
			client.sendTCP(netHello)
			
			Log.post("Host connection handshake initialized.")
			
			return true
		}
		catch(e: Exception)
		{
			Log.post("Failed to connect to localhost.", Log.Level.ERR)
			
			return false
		}
	}
	
	/*fun updatePosition()
	{
		netData.username = username
		netData.uuid = uuid
		netData.bodyX = GolfballHandler.yourGolfball!!.getBodyPosition().x
		netData.bodyY = GolfballHandler.yourGolfball!!.getBodyPosition().y
		
		client.sendUDP(netData)
	}*/
	
	fun sendShoot(degrees: Float, power: Int) //forceX: Float, forceY: Float)
	{
		netData.uuid = uuid
		/*netData.bodyX = forceX
		netData.bodyY = forceY*/
		netData.shootDegrees = degrees
		netData.shootPower = power
		
		client.sendTCP(netData)
	}
	
	fun connected(): Boolean
	{
		return client.isConnected
	}
	
	fun sendMessage(message: String)
	{
		var netMessage = NetMessage()
		
		netMessage.username = username
		netMessage.message = message
		
		try
		{
			client.sendTCP(netMessage)
		}
		catch(e: Exception)
		{
			Log.post("Failed to send message!", Log.Level.ERR)
		}
	}
}