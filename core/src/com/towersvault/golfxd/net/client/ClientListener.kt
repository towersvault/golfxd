package com.towersvault.golfxd.net.client

import com.esotericsoftware.kryonet.Connection
import com.esotericsoftware.kryonet.Listener
import com.towersvault.golfxd.game.GolfballHandler
import com.towersvault.golfxd.net.*
import com.towersvault.golfxd.screens.Menu
import com.towersvault.golfxd.screens.Scene2DChat
import com.towersvault.golfxd.screens.Scene2DHelper
import com.towersvault.golfxd.util.Log
import java.util.*

class ClientListener : Listener()
{
	override fun received(connection: Connection, obj: Any)
	{
		try
		{
			if(obj is NetUpdate)
			{
				GolfballHandler.updateGolfballs(obj)
			}
			else if(obj is NetData)
			{
				try
				{
					if(obj.data.startsWith("timer:"))
						Scene2DHelper.setWaitingText("${obj.data.substring("timer:".length)}")
				}
				catch(e: Exception)
				{
					/*GolfballHandler.otherGolfball(obj)*/
				}
			}
			else if(obj is NetHello)
			{
				NetClient.uuid = obj.uuid
				GolfballHandler.yourGolfball!!.uuid = UUID.fromString(obj.uuid)
			}
			else if(obj is NetDisconnect)
			{
				Log.post("Got disconnect request for ${obj.connectionId}:${obj.uuid}")
				//GolfballHandler.removeGolfball(obj)
			}
			else if(obj is NetMessage)
			{
				Scene2DChat.parseMessage(obj)
			}
			else if(obj is NetStart)
			{
				Log.post("Client game started!")
				GolfballHandler.resetBuffers()
				Scene2DHelper.showMenu(Menu.GAME)
			}
		}
		catch(e: Exception) {}
	}
}