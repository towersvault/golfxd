package com.towersvault.golfxd.net;

public class NetData
{
	public String uuid;
	public String username;
	/*public float bodyX;
	public float bodyY;*/
	public float shootDegrees;
	public int shootPower;
	public String data;
	public float r;
	public float g;
	public float b;
}
