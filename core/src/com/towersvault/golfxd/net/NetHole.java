package com.towersvault.golfxd.net;

/**
 * Used for transmitting new spawnpoint data to clients.
 */
public class NetHole
{
	public int hole;
	public float x;
	public float y;
}
