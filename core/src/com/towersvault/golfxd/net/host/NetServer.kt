package com.towersvault.golfxd.net.host

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Array
import com.esotericsoftware.kryonet.Server
import com.towersvault.golfxd.game.world.MapHandler
import com.towersvault.golfxd.net.*
import com.towersvault.golfxd.net.client.NetClient
import com.towersvault.golfxd.screens.CL
import com.towersvault.golfxd.util.Log

object NetServer
{
	var server = Server()
	
	val tcp = NetClient.tcp
	val udp = NetClient.udp
	
	var netUpdate = NetUpdate()
	
	private var serverOpen = false
	
	fun init()
	{
		Log.post("Initializing server.")
		
		
		com.esotericsoftware.minlog.Log.set(com.esotericsoftware.minlog.Log.LEVEL_DEBUG)
		
		var listener = ServerListener()
		
		serverOpen = false
		
		try
		{
			server.bind(tcp, udp)
			
			server.addListener(listener)
			
			server.start()
			
			var k = server.kryo
			k.register(NetData::class.java)
			k.register(NetHello::class.java)
			k.register(NetDisconnect::class.java)
			k.register(NetMessage::class.java)
			k.register(NetStart::class.java)
			k.register(NetHole::class.java)
			k.register(NetUpdate::class.java)
			k.register(NetGolfball::class.java)
			
			Log.post("Initialized server.")
		}
		catch(e: Exception)
		{
			Log.post("Failed to initialize server.")
		}
	}
	
	fun close()
	{
		Log.post("Closing server connections.")
		
		if(server.connections.size > 0)
			for(i in 0..server.connections.size - 1)
				server.connections[i].close()
		
		Log.post("Server connections closed.")
		
		serverOpen = false
	}
	
	fun open()
	{
		GameHandler.hostSession()
		
		Log.post("Opening server.")
		serverOpen = true
		Log.post("Server opened.")
		
		ClientHandler.initSession()
		NetClient.connectLocalhost()
	}
	
	fun startGame()
	{
		var start = NetStart()
		server.sendToAllTCP(start)
		
		GolfballHandlerHost.setPosition(MapHandler.spawn.x, MapHandler.spawn.y, true)
	}
	
	fun isServerOpen(): Boolean
	{
		return serverOpen
	}
	
	fun broadcastTCP(netData: NetData)
	{
		server.sendToAllTCP(netData)
	}
	
	fun broadcastUDP(netData: NetData)
	{
		server.sendToAllUDP(netData)
	}
	
	fun broadcastUDP(golfballsData: Array<NetGolfball>)
	{
		netUpdate.golfballs = ""//${GolfballHandlerHost.golfballDelta}|"
		
		if(golfballsData.size > 0)
		{
			for(i in 0..golfballsData.size - 1)
			{
				netUpdate.golfballs += "${golfballsData.get(i).uuid},${golfballsData.get(i).delta}" +
						",${golfballsData.get(i).x},${golfballsData.get(i).y}|"
			}
		}
		
		//CL.getBytes(netUpdate.golfballs)
		
		server.sendToAllUDP(netUpdate)
	}
	
	fun update()
	{
		GolfballHandlerHost.update()
	}
}