package com.towersvault.golfxd.net.host

import com.esotericsoftware.kryonet.Connection
import com.esotericsoftware.kryonet.Listener
import com.towersvault.golfxd.net.NetData
import com.towersvault.golfxd.net.NetDisconnect
import com.towersvault.golfxd.net.NetHello
import com.towersvault.golfxd.util.Log
import java.util.*

class ServerListener: Listener()
{
	override fun received(connection: Connection, obj: Any)
	{
		try
		{
			if(!NetServer.isServerOpen())
			{
				connection.close()
				Log.post("Data received from ${connection.id}, ignoring because server is closed.")
			}
			
			if(obj is NetData)
			{
				if(ClientHandler.getClient(UUID.fromString(obj.uuid)).connectionId != connection.id)
					ClientHandler.getClient(UUID.fromString(obj.uuid)).connectionId = connection.id
				
				GolfballHandlerHost.applyForce(UUID.fromString(obj.uuid), obj.shootDegrees, obj.shootPower)
				
				/*NetServer.server.sendToAllExceptUDP(connection.id, obj)*/
			}
			else if(obj is NetHello)
			{
				Log.post("Hello received from ${obj.username} [${connection.id}]")
				
				var client = ClientHandler.createClient(connection.id)
				
				obj.uuid = client.uuid.toString()
				obj.username = client.username
				
				Log.post("Connection established with ${client.username} [${connection.id}:${obj.uuid}]")
				
				NetServer.server.sendToTCP(connection.id, obj)
			}
		}
		catch(e: Exception)
		{
		
		}
	}
	
	override fun connected(connection: Connection)
	{
	
	}
	
	override fun disconnected(connection: Connection)
	{
		Log.post("Client disconnected.")
		
		var client = ClientHandler.getClient(connection.id)
		
		var disconnect = NetDisconnect()
		disconnect.username = client.username
		disconnect.uuid = client.uuid.toString()
		disconnect.connectionId = client.connectionId
		
		NetServer.server.sendToAllTCP(disconnect)
		ClientHandler.removeClient(UUID.fromString(disconnect.uuid))
	}
}