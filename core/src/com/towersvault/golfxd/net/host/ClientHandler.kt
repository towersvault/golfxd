package com.towersvault.golfxd.net.host

import com.badlogic.gdx.utils.Array
import com.towersvault.golfxd.util.Log
import java.util.*

object ClientHandler
{
	private var clients = Array<Client>()
	
	fun initSession()
	{
		clients.clear()
		
		Log.post("Client session profiler initialized.")
	}
	
	fun createClient(connectionId: Int): Client
	{
		Log.post("Connection ${connectionId} connected, creating session profile.")
		
		var client = Client(UUID.randomUUID(), connectionId)
		client.username = "Debug${connectionId}"
		clients.add(client)
		
		GolfballHandlerHost.createBall(client.uuid, 0f, 0f)
		
		return client
	}
	
	fun removeClient(uuid: UUID)
	{
		for(i in 0..clients.size)
			if(clients.get(i).uuid == uuid)
			{
				Log.post("Removing client ${clients.get(i).username} [${clients.get(i).connectionId}:${clients.get(i).uuid}]")
				GolfballHandlerHost.removeBall(uuid)
				clients.removeIndex(i)
				return
			}
	}
	
	fun getClient(connectionId: Int): Client
	{
		if(clients.size > 0)
			for(client in clients)
				if(client.connectionId == connectionId)
					return client
		
		var nullClient = Client(UUID.randomUUID(), -1)
		
		return nullClient
	}
	
	fun getClient(uuid: UUID): Client
	{
		if(clients.size > 0)
			for(client in clients)
				if(client.uuid == uuid)
					return client
		
		var nullClient = Client(UUID.randomUUID(), -1)
		
		return nullClient
	}
}