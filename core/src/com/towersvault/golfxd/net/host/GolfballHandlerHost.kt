package com.towersvault.golfxd.net.host

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Net
import com.badlogic.gdx.utils.Array
import com.towersvault.golfxd.game.GolfballHandler
import com.towersvault.golfxd.game.world.MapHandler
import com.towersvault.golfxd.net.NetUpdate
import java.util.*

object GolfballHandlerHost
{
	private var golfballs = Array<GolfballHost>()
	//private var golfballsData = Array<NetGolfball>()
	private var golfballsData = Array<NetGolfball>()
	
	var golfballDelta = 0
	
	// Physics simulation is 3 frames ahead.
	val buffer = 5
	
	var singletonForceX = 0f
	var singletonForceY = 0f
	
	fun init()
	{
		reset()
	}
	
	fun reset()
	{
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
				MapHandler.destroyBody(golfballs.get(i).body!!)
		
		golfballs.clear()
		try
		{
			golfballsData.clear()
		}
		catch(e: Exception)
		{
			golfballsData = Array<NetGolfball>()
		}
		golfballDelta = 0
	}
	
	fun createBall(uuid: UUID, x: Float, y: Float)
	{
		var golfball = GolfballHost(uuid)
		golfball.init(x, y)
		golfballs.add(golfball)
		var netGolfball = NetGolfball()
		netGolfball.uuid = golfball.uuid.toString()
		netGolfball.x = golfball.getBodyX()
		netGolfball.y = golfball.getBodyY()
		golfballsData.add(netGolfball)
		
		println("Physics body created for $uuid.")
	}
	
	fun removeBall(uuid: UUID)
	{
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
				if(golfballs.get(i).uuid == uuid)
				{
					golfballs.removeIndex(i)
					golfballsData.removeIndex(i)
				}
	}
	
	fun applyForce(uuid: UUID, degrees: Float, power: Int) //forceX: Float, forceY: Float)
	{
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
				if(golfballs.get(i).uuid == uuid)
				{
					singletonForceX = (power.toFloat() * GolfballHandler.forceStrength) * Math.cos(Math.toRadians(degrees.toDouble())).toFloat()
					singletonForceY = (power.toFloat() * GolfballHandler.forceStrength * 1.5f) * Math.sin(Math.toRadians(degrees.toDouble())).toFloat()
					
					if(singletonForceY < 0f) singletonForceY = -singletonForceY
					
					golfballs.get(i).applyForce(singletonForceX, singletonForceY)
				}
	}
	
	fun setPosition(x: Float, y: Float)
	{
		for(i in 0..golfballs.size - 1)
		{
			golfballs.get(i).setPosition(x, y)
		}
	}
	
	fun setPosition(x: Float, y: Float, resetForce: Boolean)
	{
		for(i in 0..golfballs.size - 1)
		{
			golfballs.get(i).setPosition(x, y, resetForce)
		}
	}
	
	fun update()
	{
		golfballDelta += 1
		
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
			{
				golfballs.get(i).update()
				
				golfballsData.get(i).delta = golfballDelta
				golfballsData.get(i).x = golfballs.get(i).getBodyX()
				golfballsData.get(i).y = golfballs.get(i).getBodyY()
			}
		
		NetServer.broadcastUDP(golfballsData)
		
		/*
		* Latency issue:
		*
		* synchedDelta at every rendered frame.
		*
		* 878.0
		* 878.0
		* 880.0
		* 881.0
		* 882.0
		* 882.0
		* 884.0
		* 885.0
		* 885.0
		* 887.0
		* 888.0
		* 889.0
		* 890.0
		*
		* Perhaps add buffer with 10 frames of data.
		*
		* */
	}
}