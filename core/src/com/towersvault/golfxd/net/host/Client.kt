package com.towersvault.golfxd.net.host

import com.badlogic.gdx.utils.Array
import java.util.*

class Client(var uuid: UUID, var connectionId: Int)
{
	var username = ""
	var scores = Array<Int>()
}