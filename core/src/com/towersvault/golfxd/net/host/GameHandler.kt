package com.towersvault.golfxd.net.host

import com.badlogic.gdx.utils.Timer
import com.towersvault.golfxd.game.WaitingTask

object GameHandler
{
	val lobbyWaitTime = 8
	var timerLength = lobbyWaitTime
	
	fun init()
	{
	}
	
	fun hostSession()
	{
		timerLength = lobbyWaitTime
		Timer.schedule(WaitingTask, 1f, 1f, lobbyWaitTime)
		GolfballHandlerHost.reset()
	}
	
	fun hostCloseSession()
	{
		WaitingTask.cancel()
	}
}