package com.towersvault.golfxd.net.host

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.CircleShape
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.towersvault.golfxd.game.world.MapHandler
import java.util.*

class GolfballHost(val uuid: UUID)
{
	private var bodyX = 0f
	private var bodyY = 0f
	private var forceX = 0f
	private var forceY = 0f
	private var resetForce = false
	
	var body: Body? = null
	
	fun init(x: Float, y: Float)
	{
		var bd = BodyDef()
		bd.type = BodyDef.BodyType.DynamicBody
		bd.gravityScale = 1f
		bd.position.set(x, y)
		
		body = MapHandler.addBody(bd)
		
		var shape = CircleShape()
		
		shape.radius = 2f / MapHandler.tileWidth
		
		var fd = FixtureDef()
		fd.shape = shape
		fd.restitution = 0.5f
		fd.friction = 0.3f
		
		body!!.createFixture(fd)
		body!!.isSleepingAllowed = false
		body!!.isFixedRotation = false
		
		shape.dispose()
	}
	
	fun getBodyX(): Float
	{
		return body!!.position.x
	}
	
	fun getBodyY(): Float
	{
		return body!!.position.y
	}
	
	fun setPosition(bodyX: Float, bodyY: Float)
	{
		this.bodyX = bodyX
		this.bodyY = bodyY
	}
	
	fun setPosition(bodyX: Float, bodyY: Float, resetForce: Boolean)
	{
		this.bodyX = bodyX
		this.bodyY = bodyY
		this.resetForce = resetForce
	}
	
	fun applyForce(forceX: Float, forceY: Float)
	{
		this.forceX = forceX
		this.forceY = forceY
	}
	
	fun update()
	{
		if(resetForce)
		{
			resetForce = false
			body!!.setLinearVelocity(0f, 0f)
		}
		
		if(bodyX + bodyY != 0f)
		{
			body!!.setTransform(bodyX, bodyY, body!!.angle)
			bodyX = 0f
			bodyY = 0f
		}
		
		if(forceX + forceY != 0f)
		{
			body!!.applyForceToCenter(forceX * MapHandler.ppm, forceY * MapHandler.ppm, true)
			forceX = 0f
			forceY = 0f
		}
	}
}