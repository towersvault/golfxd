package com.towersvault.golfxd

import com.badlogic.gdx.Application
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.towersvault.golfxd.screens.MainScreen
import com.towersvault.golfxd.util.AspectRatioHelper

class MainGolfxD() : Game()
{
	override fun create()
	{
		when(Gdx.app.type)
		{
			Application.ApplicationType.Android -> AspectRatioHelper.inst.baseRatio = Gdx.graphics.width.toFloat() / Gdx.graphics.height.toFloat()
		}
		
		setScreen(MainScreen(this))
	}
	
}