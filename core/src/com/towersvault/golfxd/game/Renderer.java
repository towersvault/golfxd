package com.towersvault.golfxd.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.towersvault.golfxd.game.world.MapHandler;
import com.towersvault.golfxd.net.host.GameHandler;

/**
 * Created by Clifford on 2017/05/22.
 */
public class Renderer
{
	public static final Renderer inst = new Renderer();
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Controller controller;
	
	public void init(Controller controller)
	{
		this.controller = controller;
		controller.init();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(0f, 0f, 0f);
		camera.update();
		
		batch = new SpriteBatch();
	}
	
	public void translateCamera(Float translateX, Float translateY)
	{
		camera.position.set(translateX, translateY, 0f);
		camera.update();
	}
	
	public void positionCamera(Float positionX, Float positionY)
	{
		camera.position.set(positionX, positionY, 0f);
		camera.update();
	}
	
	public void render()
	{
		MapHandler.INSTANCE.render(camera);
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		controller.render(batch);
		//MapHandler.INSTANCE.renderDebug(batch);
		
		batch.end();
		
		try
		{
			camera.position.set(GolfballHandler.INSTANCE.getYourGolfball().getSpriteX(), GolfballHandler.INSTANCE.getYourGolfball().getSpriteY(), 0f);
			camera.update();
		}
		catch(Exception e) {}
		
		//renderDebug();
	}
	
	private void renderDebug()
	{
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		MapHandler.INSTANCE.renderDebug(batch);
		
		batch.end();
	}
}
