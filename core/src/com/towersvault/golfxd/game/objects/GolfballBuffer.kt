package com.towersvault.golfxd.game.objects

class GolfballBuffer(val frame: Int, val x: Float, val y: Float)