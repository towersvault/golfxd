package com.towersvault.golfxd.game.objects

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Array
import com.towersvault.golfxd.game.world.MapHandler
import com.towersvault.golfxd.net.host.GolfballHandlerHost
import com.towersvault.golfxd.screens.Menu
import com.towersvault.golfxd.screens.Scene2DHelper
import com.towersvault.golfxd.util.Assets
import com.towersvault.golfxd.util.Constants
import com.towersvault.golfxd.util.MathHelper
import java.util.*

class Golfball(val username: String, var uuid: UUID)
{
	private var sprite = Sprite()
	private var ballcolor = Sprite()
	
	private var bodyX = 0f
	private var bodyY = 0f
	
	var buffer = Array<GolfballBuffer>()
	
	var deltaUpdate = 0
	
	fun init()
	{
		sprite = Sprite(Assets.sprite!!.golfBallWhite)
		sprite.setSize(sprite.width * Constants.resize(), sprite.height * Constants.resize())
		sprite.setPosition(0f, 0f)
		
		ballcolor = Sprite(Assets.sprite!!.pxWhite)
		ballcolor.setSize(MathHelper.inst.pxToScreen(2f), MathHelper.inst.pxToScreen(2f))
		ballcolor.x = sprite.x + MathHelper.inst.pxToScreen(1f)
		ballcolor.y = sprite.y + MathHelper.inst.pxToScreen(1f)
		
		deltaUpdate = 0
		
		for(i in 0..GolfballHandlerHost.buffer - 1)
			buffer.add(GolfballBuffer(0, 0f, 0f))
	}
	
	fun getSpriteX(): Float
	{
		return sprite.x + sprite.width / 2f
	}
	
	fun getSpriteY(): Float
	{
		return sprite.y + sprite.height / 2f
	}
	
	fun setPosition(deltaUpdate: Int, x: Float, y: Float)
	{
		/*if(this.deltaUpdate < deltaUpdate)
		{
			this.deltaUpdate = deltaUpdate
			sprite.x = x * MapHandler.ppm - (sprite.width / 2f)
			sprite.y = y * MapHandler.ppm - (sprite.height / 2f)
			ballcolor.x = sprite.x + MathHelper.inst.pxToScreen(1f)
			ballcolor.y = sprite.y + MathHelper.inst.pxToScreen(1f)
		}*/
		
		if(buffer.last().frame < deltaUpdate && Scene2DHelper.currentMenu == Menu.GAME)
		{
			buffer.add(GolfballBuffer(deltaUpdate, x, y))
			//println("Buffer added: ${deltaUpdate}")
		}
	}
	
	private fun setSpritePosition(x: Float, y: Float)
	{
		sprite.x = x * MapHandler.ppm - (sprite.width / 2f)
		sprite.y = y * MapHandler.ppm - (sprite.height / 2f)
		ballcolor.x = sprite.x + MathHelper.inst.pxToScreen(1f)
		ballcolor.y = sprite.y + MathHelper.inst.pxToScreen(1f)
	}
	
	fun setBallColor(r: Float, g: Float, b: Float)
	{
		ballcolor.setColor(r, g, b, ballcolor.color.a)
	}
	
	fun resetBuffer()
	{
		buffer.clear()
		
		for(i in 0..GolfballHandlerHost.buffer - 1)
			buffer.add(GolfballBuffer(0, 0f, 0f))
	}
	
	fun render(batch: SpriteBatch)
	{
		if(Scene2DHelper.currentMenu == Menu.GAME)
		{
			setSpritePosition(buffer.get(0).x, buffer.get(0).y)
			buffer.removeIndex(0)
			
			sprite.draw(batch)
			ballcolor.draw(batch)
		}
	}
}