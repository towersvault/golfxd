package com.towersvault.golfxd.game

enum class Penalty(val penaltyName: String, val drawable: String, val descDrawable: String)
{
	POINTS_X10("Points +10", "ui_host_map_penalty_points", "ui_penalty_desc_points")
}