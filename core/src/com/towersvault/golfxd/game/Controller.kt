package com.towersvault.golfxd.game

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.towersvault.golfxd.game.world.MapHandler
import com.towersvault.golfxd.net.host.GameHandler

class Controller
{
	fun init()
	{
		GolfballHandler.init()
		//GolfballHandler.yourGolfball!!.setBodyPosition(MapHandler.spawn.x, MapHandler.spawn.y)
	}
	
	fun render(batch: SpriteBatch)
	{
		GolfballHandler.render(batch)
	}
}