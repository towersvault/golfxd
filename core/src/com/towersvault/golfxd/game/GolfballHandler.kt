package com.towersvault.golfxd.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Timer
import com.towersvault.golfxd.game.objects.Golfball
import com.towersvault.golfxd.net.NetData
import com.towersvault.golfxd.net.NetDisconnect
import com.towersvault.golfxd.net.NetUpdate
import com.towersvault.golfxd.net.client.NetClient
import com.towersvault.golfxd.net.host.NetGolfball
import com.towersvault.golfxd.screens.Scene2DHelper
import com.towersvault.golfxd.util.Constants
import java.util.*

object GolfballHandler
{
	var yourGolfball: Golfball? = null
	var golfballs = Array<Golfball>()
	
	var powerbarCharge = 1
	val powerbarMax = 13
	private var increasePower = true
	private var chargeDelta = 0f
	var canCharge = false
	
	// Used by golfballs. Bigger = moar stronk
	val forceStrength = 9f / Constants.resize()
	
	private var forceX = 0f
	private var forceY = 0f
	
	private var synchedDelta = 0f
	
	private var updateData = List<String>(1, init = { "" })
	private var singleData = List<String>(4, init = { "" })
	
	fun init()
	{
		yourGolfball = Golfball("test", UUID.randomUUID())
		yourGolfball!!.init()
		//yourGolfball!!.setBallColor(1f, 0f, 1f)
	}
	
	fun shootBall(degrees: Float)
	{
		/*forceX = (powerbarCharge.toFloat() * GolfballHandler.forceStrength) * Math.cos(Math.toRadians(degrees.toDouble())).toFloat()
		forceY = (powerbarCharge.toFloat() * GolfballHandler.forceStrength * 1.5f) * Math.sin(Math.toRadians(degrees.toDouble())).toFloat()
		
		if (forceY < 0f)
			forceY = -forceY
		
		NetClient.sendShoot(forceX, forceY)*/
		
		NetClient.sendShoot(degrees, powerbarCharge)
	}
	
	fun updateGolfballs(netUpdate: NetUpdate)
	{
		updateData = netUpdate.golfballs.split("|")
		
		/*println("-------------")
		for(i in 0..updateData.size)
		{
			println("Data: ${updateData[i]}")
		}*/
		
		if(updateData.size > 0)
		{
			for(i in 0..updateData.size - 1)
			{
				singleData = updateData[i].split(",")
				
				try
				{
					if (synchedDelta < singleData[1].toFloat())
						synchedDelta = singleData[1].toFloat()
					
					//println("${yourGolfball!!.uuid.toString()} - ${singleData[0]}")
					
					if (singleData[0] == yourGolfball!!.uuid.toString())
					{
						yourGolfball!!.setPosition(singleData[1].toInt(), singleData[2].toFloat(), singleData[3].toFloat())
						//break@localLoop
					} else if (golfballs.size > 0)
					{
						localLoop@ for (local in golfballs)
						{
							/*try
							{*/
							/*if(synchedDelta < singleData[1].toFloat())
									synchedDelta = singleData[1].toFloat()*/
							
							/*if(singleData[0] == yourGolfball!!.uuid.toString())
								{
									yourGolfball!!.setPosition(singleData[1].toFloat(), singleData[2].toFloat(), singleData[3].toFloat())
									break@localLoop
								}*/
							/*else
								{*/
							if (singleData[0] == local.uuid.toString())
							{
								local.setPosition(singleData[1].toInt(), singleData[2].toFloat(), singleData[3].toFloat())
								break@localLoop
							} else
							{
								createBall(singleData)
								break@localLoop
							}
							/*}*/
							/*} catch(e: Exception)
							{
								e.printStackTrace()
								Gdx.app.exit()
							}*/
						}
					} else if(updateData[i].length > 0)
					{
						createBall(singleData)
					}
				}
				catch(e: Exception)
				{
				
				}
			}
		}
		
		/*if(golfballs.size > 0)
		{
			for(net in netUpdate.golfballs)
			{
				if(synchedDelta < net.delta)
					synchedDelta = net.delta
				
				for(local in golfballs)
				{
					if(net.uuid == yourGolfball!!.uuid.toString())
					{
						yourGolfball!!.setPosition(net.delta, net.x, net.y)
					}
					else
					{
						if(local.uuid.toString() == net.uuid)
						{
							local.setPosition(net.delta, net.x, net.y)
							net.clientUpdated = true
						}
					}
				}
				
				if(!net.clientUpdated)
				{
					createBall(net)
					net.clientUpdated = true
				}
			}
		}*/
	}
	
	private fun createBall(netGolfball: NetGolfball)
	{
		var golfball = Golfball("", UUID.fromString(netGolfball.uuid))
		golfball.setPosition(netGolfball.delta, netGolfball.x, netGolfball.y)
		golfballs.add(golfball)
	}
	
	private fun createBall(data: List<String>)
	{
		/*if(data[0].length > 0 && data[1].length > 0)
			println("Create: ${data[0]} ${data[1]}")*/
		
		if(data[0].length > 0 && data[1].length > 0)
		{
			var golfball = Golfball("", UUID.fromString(data[0]))
			golfball.init()
			golfball.setPosition(data[1].toInt(), data[2].toFloat(), data[3].toFloat())
			golfballs.add(golfball)
		}
	}
	
	/*fun otherGolfball(netData: NetData)
	{
		//println("${netData.bodyX}, ${netData.bodyY} - ${netData.uuid}")
		
		if(golfballs.size > 0)
		{
			for(i in 0..golfballs.size - 1)
			{
				if(golfballs.get(i).uuid == UUID.fromString(netData.uuid))
				{
					golfballs.get(i).setBodyPosition(netData.bodyX, netData.bodyY)
					return
				}
			}
		}
		
		var golfball = Golfball(netData.username, UUID.fromString(netData.uuid))
		golfball.init()
		golfball.setBallColor(netData.r, netData.g, netData.b)
		//golfball.setBodyPosition(netData.bodyX, netData.bodyY)
		golfball.setPosition(netData.bodyX, netData.bodyY)
		golfballs.add(golfball)
	}*/
	
	/*fun removeGolfball(netDisconnect: NetDisconnect)
	{
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
				if(golfballs.get(i).uuid == UUID.fromString(netDisconnect.uuid))
					golfballs.removeIndex(i)
	}*/
	
	fun resetBuffers()
	{
		try
		{
			yourGolfball!!.resetBuffer()
		}
		catch(e: Exception) {}
		
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
				golfballs.get(i).resetBuffer()
	}
	
	fun render(batch: SpriteBatch)
	{
		chargeDelta += Gdx.graphics.deltaTime
		
		if(canCharge && chargeDelta > 1f / 25f)
		{
			if(powerbarCharge <= 1)
				increasePower = true
			else if(powerbarCharge >= powerbarMax)
				increasePower = false
			
			if(increasePower) powerbarCharge++
			else powerbarCharge --
			
			chargeDelta = 0f
			
			Scene2DHelper.setChargeBarPower(powerbarCharge)
		}
		
		if(golfballs.size > 0)
			for(i in 0..golfballs.size - 1)
			{
				try
				{
					//if(golfballs.get(i).deltaUpdate >= synchedDelta)
					//if(golfballs.get(i).buffer.size > 0)
					//{
						golfballs.get(i).render(batch)
					//}
					//else
					//	golfballs.removeIndex(i)
				}
				catch(e: Exception) {}
			}
		
		try
		{
			yourGolfball!!.render(batch)
		}
		catch(e: Exception) {}
		
		/*try
		{
			yourGolfball!!.render(batch)
		}
		catch(e: Exception) {}
		
		if(golfballs.size > 0)
			for(i in 0..golfballs.size)
			{
				try
				{
					golfballs.get(i).render(batch)
				}
				catch(e: Exception) {}
			}*/
	}
}