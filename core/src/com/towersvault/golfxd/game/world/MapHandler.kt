package com.towersvault.golfxd.game.world

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapRenderer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Disposable
import com.towersvault.golfxd.util.Constants
import com.towersvault.golfxd.util.Log

object MapHandler : Disposable
{
	enum class MapData(val title: String, val filename: String)
	{
		DEBUG("Debug", "debug")
	}
	
	var world: World? = null
	var debugRenderer = Box2DDebugRenderer()
	var debugMatrix: Matrix4? = null
	
	var map: TiledMap = TiledMap()
	var renderer: TiledMapRenderer? = null
	
	val tileWidth = 8f
	var ppm = 0f
	
	var spawn: Vector2 = Vector2()
	var flag: Vector2 = Vector2()
	
	fun init()
	{
		ppm = tileWidth * Constants.resize()
		
		var gravityScale = Vector2(0f, 1f)
		world = World(gravityScale, false)
	}
	
	fun loadMap(mapData: MapData)
	{
		Log.post("Loading map " + mapData.title)
		
		if(map != null)
			map.dispose()
		
		map = TmxMapLoader().load("maps/" + mapData.filename + ".tmx")
		renderer = OrthogonalTiledMapRenderer(map, Constants.resize())
		
		var layer = map.layers.get(map.layers.count() - 2) as TiledMapTileLayer
		
		for(x in 0..layer.width)
		{
			for(y in 0..layer.height)
			{
				try
				{
					if (layer.getCell(x, y).tile.properties.get("data", String::class.java) != null)
					{
						var property = layer.getCell(x, y).tile.properties.get("data", String::class.java)
						
						if (property == "spawn")
							spawn.set(x.toFloat(), y.toFloat())
						else if (property == "flag")
							flag.set(x.toFloat(), y.toFloat())
					}
				}
				catch(e: Exception) {}
			}
		}
		
		loadBox2D()
		
		Log.post("Done loading map! Spawn(${spawn.x}, ${spawn.y}) Flag(${flag.x}, ${flag.y})")
	}
	
	fun loadBox2D()
	{
		if(world != null)
			world!!.dispose()
		
		var gravity = Vector2(0f, -9.8f)
		world = World(gravity, false)
		
		MapBodyBuilder.buildShapes(map, 8f, world, "Bodies")
		
		Log.post("Loaded map physics bodies.")
	}
	
	fun addBody(bd: BodyDef): Body
	{
		var loadedBody = world!!.createBody(bd)
		
		return loadedBody
	}
	
	fun destroyBody(body: Body)
	{
		try
		{
			world!!.destroyBody(body)
		}
		catch(e: Exception)
		{
			e.printStackTrace()
		}
	}
	
	fun render(camera: OrthographicCamera)
	{
		world!!.step(1f / 60f, 8, 8)
		
		if(renderer != null)
		{
			renderer!!.setView(camera)
			renderer!!.render()
		}
	}
	
	fun renderDebug(batch: SpriteBatch)
	{
		debugMatrix = batch.projectionMatrix.cpy().scale(ppm, ppm, 0f)
		debugRenderer.render(world, debugMatrix)
	}
	
	override fun dispose()
	{
	
	}
}