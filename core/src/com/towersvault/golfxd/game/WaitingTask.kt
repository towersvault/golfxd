package com.towersvault.golfxd.game

import com.badlogic.gdx.utils.Timer
import com.towersvault.golfxd.net.NetData
import com.towersvault.golfxd.net.NetStart
import com.towersvault.golfxd.net.host.GameHandler
import com.towersvault.golfxd.net.host.NetServer
import com.towersvault.golfxd.screens.Scene2DHelper
import com.towersvault.golfxd.util.Log

object WaitingTask: Timer.Task()
{
	override fun run()
	{
		//Scene2DHelper.setWaitingText("${GameHandler.timerLength}")
		var netData = NetData()
		netData.data = "timer:${GameHandler.timerLength}"
		NetServer.broadcastTCP(netData)
		GameHandler.timerLength -= 1
		
		if(GameHandler.timerLength == -1)
		{
			Log.post("Host game started!")
			NetServer.startGame()
		}
	}
	
	override fun cancel()
	{
		super.cancel()
		GameHandler.timerLength = GameHandler.lobbyWaitTime
	}
}