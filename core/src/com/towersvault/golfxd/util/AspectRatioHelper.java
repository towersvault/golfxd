package com.towersvault.golfxd.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

public class AspectRatioHelper
{
	public final static AspectRatioHelper inst = new AspectRatioHelper();
	
	private int resize;
	
	//return (float)((int)(Gdx.graphics.getHeight() / 180f));
	
	public float baseRatio;
	
	private int ratioWidth;
	private int ratioHeight;
	
	public static final float GAME_SCREEN_HEIGHT = 130f;
	
	public void findAspectRatio()
	{
		findRatios:
			for(int i = 1; i > -1; i++)
			{
				ratioHeight = i;
				ratioWidth = (int)(i * baseRatio);
				
				try
				{
					if (Gdx.graphics.getHeight() / ratioHeight == Gdx.graphics.getWidth() / ratioWidth)
					{
						break findRatios;
					}
				}
				catch(ArithmeticException e)
				{
					e.printStackTrace();
					break findRatios;
				}
			}
		
		/*if(!Constants.INSTANCE.getReleaseMode())
			System.out.println("Aspect Ratio=" + ratioWidth + ":" + ratioHeight);*/
	
		switch(Gdx.app.getType())
		{
			case Desktop:
				resize = ((int)(Gdx.graphics.getHeight() / GAME_SCREEN_HEIGHT));
				break;
			case Android:
				resize = ((int)(Gdx.graphics.getHeight() / (GAME_SCREEN_HEIGHT - 30f)));
				break;
			default:
				resize = ((int)(Gdx.graphics.getHeight() / GAME_SCREEN_HEIGHT));
				break;
		}
		
		if(resize % 2 == 1)
			resize += 1;
		
		Log.post("Aspect ratio: " + ratioWidth + ":" + ratioHeight);
		Log.post("Resize scale: " + resize);
	}
	
	public float getResize()
	{
		return (float)resize;
	}
	
	public int getRatioWidth()
	{
		return ratioWidth;
	}
	
	public int getRatioHeight()
	{
		return ratioHeight;
	}
}
