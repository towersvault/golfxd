package com.towersvault.golfxd.util

import com.badlogic.gdx.Gdx
import com.towersvault.golfxd.util.AspectRatioHelper
import java.net.Inet4Address
import java.net.NetworkInterface
import java.util.*
import kotlin.collections.ArrayList

object Constants
{
	val r = 1f
	val g = 1f
	val b = 1f
	val a = 1f
	
	val spritesJson = "sprites/sprites.json"
	val spritesAtlas = "sprites/sprites.atlas"
	
	val devName = "towersvault"
	val gameName = "Golf xD"
	val version = "v0.1.0"
	
	val releaseMode = false
	val showUi = true
	
	val cal = Calendar.getInstance()
	
	val second = cal.get(Calendar.SECOND)
	val minute = cal.get(Calendar.MINUTE)
	val hour = cal.get(Calendar.HOUR)
	
	val day = cal.get(Calendar.DAY_OF_MONTH)
	val month = cal.get(Calendar.MONTH) + 1
	val year = cal.get(Calendar.YEAR)
	
	var ipAddress: String = getIp()
	
	private fun getIp(): String
	{
		if(ipAddress != null)
			return ipAddress
		
		var addresses = ArrayList<String>()
		
		try
		{
			var interfaces = NetworkInterface.getNetworkInterfaces()
			
			for(ni in Collections.list(interfaces))
			{
				for(address in Collections.list(ni.inetAddresses))
				{
					if(address is Inet4Address)
						addresses.add(address.hostAddress)
				}
			}
		}
		catch(e: Exception)
		{
			e.printStackTrace()
			Gdx.app.exit()
		}
		
		for(address in addresses)
		{
			Log.post("Host IP Found: $address")
			
			if(address != "127.0.0.1")
				ipAddress = address
		}
		
		return ipAddress
	}
	
	
	fun resize(): Float
	{
		return AspectRatioHelper.inst.resize
	}
}