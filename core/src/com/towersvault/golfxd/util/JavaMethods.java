package com.towersvault.golfxd.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class JavaMethods
{
	public static final JavaMethods inst = new JavaMethods();
	
	public void setInputProcessor(Stage stage)
	{
		Gdx.input.setInputProcessor(stage);
	}
}
