package com.towersvault.golfxd.util;

import com.towersvault.golfxd.util.Constants;

public class Log
{
	private static final boolean POST_OUTPUT = true;
	
	public enum Level
	{
		STD("S"),
		ERR("E");
		
		public String legend;
		
		Level(String legend)
		{
			this.legend = legend;
		}
	}
	
	public static void post(String message)
	{
		post(message, Level.STD);
	}
	
	public static void post(String message, Level logLevel)
	{
		if(!POST_OUTPUT)
			return;
			
			if(logLevel.equals(Level.ERR))
		{
			System.err.println("E: " + message + "\n" + Constants.INSTANCE.getHour() + ":"
				+ Constants.INSTANCE.getMinute() + ":" + Constants.INSTANCE.getSecond() + " "
				+ Constants.INSTANCE.getDay() + "-" + Constants.INSTANCE.getMonth() + "-" + Constants.INSTANCE.getYear());
			return;
		}
		
		System.out.println(logLevel.legend + ": " + message);
	}
	
	public static void error(String message)
	{
		post(message, Level.ERR);
	}
}
