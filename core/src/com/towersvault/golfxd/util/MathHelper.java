package com.towersvault.golfxd.util;

import com.badlogic.gdx.Gdx;
import com.towersvault.golfxd.util.Constants;

/*
 *     /\__/\
 *    /`    '\
 *  === 0  0 ===
 *    \  --  /
 *   /        \
 *  /          \
 * |            |
 *  \  ||  ||  /
 *   \_oo__oo_/#######o
 *   
 * Looking through code is fun, is it not?
 * 
 * Please don't leak any information from the code.
 * 
 * Thanks! :)
 */

public class MathHelper
{
	public static final MathHelper inst = new MathHelper();
	
	public float pxToScreen(float px)
	{
		return px * Constants.INSTANCE.resize();
	}
	
	public float pxToScreen(float px, boolean fromTop)
	{
		return Gdx.graphics.getHeight() - pxToScreen(px);
	}
	
	public float translate(float coord)
	{
		return coord * (8f * Constants.INSTANCE.resize());
	}
	
	public float round(float number)
	{
		if(number % 2 == 1)
			number -= 1;
		
		return  number;
	}
	
	public float biggestPrime(float number)
	{
		float biggestPrime = number;
		
		for(int i = 0; i < number - 1; i++)
		{
			if(number % i == 0)
				biggestPrime = i;
		}
		
		return biggestPrime;
	}
}
