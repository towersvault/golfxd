package com.towersvault.golfxd.util

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetErrorListener
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.utils.Disposable

object Assets : Disposable, AssetErrorListener
{
	private var assetManager: AssetManager = AssetManager()
	
	var sprite: AssetSprite? = null
	
	class AssetSprite(atlas: TextureAtlas)
	{
		val golfBallWhite = atlas.findRegion("spr_golfball_white")
		val pxWhite = atlas.findRegion("px_white")
	}
	
	fun init(assetManager: AssetManager)
	{
		this.assetManager = assetManager
		
		assetManager.setErrorListener(this)
		assetManager.load(Constants.spritesAtlas, TextureAtlas::class.java)
		assetManager.finishLoading()
		
		var atlas: TextureAtlas = assetManager.get(Constants.spritesAtlas)
		
		for(texture in atlas.textures)
			texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)
	
		sprite = AssetSprite(atlas)
		
		Log.post("Initialized assets.")
	}
	
	override fun dispose()
	{
		assetManager.dispose()
	}
	
	override fun error(asset: AssetDescriptor<*>?, throwable: Throwable?)
	{
	
	}
}