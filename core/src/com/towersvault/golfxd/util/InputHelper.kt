package com.towersvault.golfxd.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

object InputHelper
{
	fun check()
	{
		if(Gdx.input.isKeyJustPressed(Input.Keys.F2))
			ScreenshotHandler.saveScreenshot()
	}
}