package com.towersvault.golfxd.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.utils.viewport.StretchViewport
import com.towersvault.golfxd.game.Controller
import com.towersvault.golfxd.game.Renderer
import com.towersvault.golfxd.game.world.MapHandler
import com.towersvault.golfxd.net.client.NetClient
import com.towersvault.golfxd.net.host.NetServer
import com.towersvault.golfxd.util.*

class MainScreen(val game: Game) : AbstractGameScreen(game)
{
	var stage = Stage()
	var stack = Stack()
	
	private fun init()
	{
		stage.clear()
		stack.clear()
		
		stage.addActor(stack)
		stack.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		
		addStackItems()
	}
	
	private fun addStackItems()
	{
		Scene2DHelper.init()
		
		stack.add(Scene2DHelper.buildDebug())
		
		stack.add(Scene2DHelper.buildMainMenu())
		stack.add(Scene2DHelper.buildColorPicker())
		stack.add(Scene2DHelper.buildGame())
		stack.add(Scene2DHelper.buildPlay())
		stack.add(Scene2DHelper.buildHost())
		stack.add(Scene2DHelper.buildJoin())
		stack.add(Scene2DHelper.buildLobby())
		
		//stack.add(Scene2DHelper.buildBallInteraction())
		
		//Scene2DChat.init()
		//stack.add(Scene2DChat.build())
		
		Scene2DHelper.showMenu(Menu.COLOR_PICKER)
	}
	
	override fun resize(width: Int, height: Int)
	{
		stage.viewport.update(width, height, true)
	}
	
	override fun render(deltaTime: Float)
	{
		InputHelper.check()
		
		Gdx.gl.glClearColor(Constants.r, Constants.g, Constants.b, Constants.a)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
		
		Scene2DHelper.update()
		
		if(NetServer.isServerOpen())
			NetServer.update()
		
		Renderer.inst.render()
		
		stage.act(deltaTime)
		stage.draw()
		
	}
	
	override fun show()
	{
		AspectRatioHelper.inst.findAspectRatio()
		
		stage = Stage(StretchViewport(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat()))
		stack = Stack()
		
		Assets.init(AssetManager())
		
		init()
		
		JavaMethods.inst.setInputProcessor(stage)
		
		MapHandler.init()
		MapHandler.loadMap(MapHandler.MapData.DEBUG)
	
		var controller = Controller()
		Renderer.inst.init(controller)
		
		NetServer.init()
		NetClient.init()
		
		Log.post("IP: ${Constants.ipAddress}")
	}
	
	override fun hide()
	{
		stage.dispose()
	}
	
	override fun pause()
	{
	
	}
	
}