package com.towersvault.golfxd.screens

import android.transition.Scene
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Timer
import com.towersvault.golfxd.game.GolfballHandler
import com.towersvault.golfxd.game.Penalty
import com.towersvault.golfxd.game.ShootTask
import com.towersvault.golfxd.net.client.NetClient
import com.towersvault.golfxd.net.host.ClientHandler
import com.towersvault.golfxd.util.Constants
import com.towersvault.golfxd.util.MathHelper

object Scene2DHelper
{
	var skin = Skin()
	
	private var layerMainMenu = Table()
	private var layerPlay = Table()
	private var layerHost = Table()
	private var layerJoin = Table()
	private var layerLobby = Table()
	private var layerGame = Table()
	private var layerGameScores = Table()
	private var layerColorPicker = Table()
	
	public var currentMenu = Menu.MAIN_MENU
	
	private var lblStrokes: Label? = null
	
	// LayerJoin's UI text stuff.
	private var int1: Image? = null
	private var counter1 = 1
	private var int2: Image? = null
	private var counter2 = 2
	private var int3: Image? = null
	private var counter3 = 3
	public var lobbyId = "000"
	private var connecting: Label? = null
	
	private var arrow: Image? = null
	private var arrowClockwise = false
	private var point: Button? = null
	private var shoot: Button? = null
	private var powerbar: Image? = null
	public var donePointing = false
	
	private var waitingText: Label? = null
	
	// LayerColorPicker
	private var pickerR: Image? = null
	private var pickerG: Image? = null
	private var pickerB: Image? = null
	private var ballColor: Image? = null
	private var pickerMaxLeft = 0f
	private var pickerMaxRight = 0f
	var colorRVal = 1f
	var colorGVal = 1f
	var colorBVal = 1f
	
	fun init()
	{
		skin = Skin(Gdx.files.internal(Constants.spritesJson),
				TextureAtlas(Gdx.files.internal(Constants.spritesAtlas)))
	
		CL.inst.init()
		
		lobbyId = "$counter1$counter2$counter3"
	}
	
	fun buildMainMenu(): Table
	{
		layerMainMenu.clear()
		
		layerMainMenu.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerMainMenu.add(background)
		
		var mainmenu = Image(skin, "ui_main_menu")
		mainmenu.setSize(mainmenu.width * Constants.resize(), mainmenu.height * Constants.resize())
		layerMainMenu.add(mainmenu)
		mainmenu.x = (Gdx.graphics.width / 2f) - (mainmenu.width / 2f)
		mainmenu.y = (Gdx.graphics.height - mainmenu.height) - MathHelper.inst.pxToScreen(10f)
		
		var play = Button(skin, "play")
		play.setSize(play.width * Constants.resize(), play.height * Constants.resize())
		layerMainMenu.add(play)
		play.x = (mainmenu.x + (mainmenu.width / 2f)) - (play.width / 2f)
		play.y = mainmenu.y - MathHelper.inst.pxToScreen(24f)
		play.addListener(CL.inst.menuPlay)
		
		var options = Button(skin, "options")
		options.setSize(options.width * Constants.resize(), options.height * Constants.resize())
		layerMainMenu.add(options)
		options.x = play.x
		options.y = play.y - (MathHelper.inst.pxToScreen(5f) + options.height)
		options.addListener(CL.inst.menuOptions)
		
		var exit = Button(skin, "exit")
		exit.setSize(exit.width * Constants.resize(), exit.height * Constants.resize())
		layerMainMenu.add(exit)
		exit.x = options.x
		exit.y = options.y - (2 * (MathHelper.inst.pxToScreen(5f) + options.height))
		exit.addListener(CL.inst.menuExit)
		
		var version = Label("version ${Constants.version} - (c) ${Constants.devName} 2017", skin, "default")
		version.setFontScale(Constants.resize() / 2f)
		layerMainMenu.add(version)
		version.x = MathHelper.inst.pxToScreen(5f)
		version.y = MathHelper.inst.pxToScreen(5f)
		
		layerMainMenu.isVisible = false
		
		return layerMainMenu
	}
	
	fun buildPlay(): Table
	{
		layerPlay.clear()
		
		layerPlay.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerPlay.add(background)
		
		var play = Image(skin, "ui_play")
		play.setSize(play.width * Constants.resize(), play.height * Constants.resize())
		layerPlay.add(play)
		play.x = (Gdx.graphics.width / 2f) - (play.width / 2f)
		play.y = (Gdx.graphics.height - play.height) - MathHelper.inst.pxToScreen(10f)
		
		var host = Button(skin, "host")
		host.setSize(host.width * Constants.resize(), host.height * Constants.resize())
		layerPlay.add(host)
		host.x = (play.x + (play.width / 2f)) - (host.width / 2f)
		host.y = play.y - MathHelper.inst.pxToScreen(24f)
		host.addListener(CL.inst.playHost)
		
		var join = Button(skin, "join")
		join.setSize(join.width * Constants.resize(), join.height * Constants.resize())
		layerPlay.add(join)
		join.x = host.x
		join.y = host.y - (MathHelper.inst.pxToScreen(5f) + join.height)
		join.addListener(CL.inst.playJoin)
		
		var back = Button(skin, "back")
		back.setSize(back.width * Constants.resize(), back.height * Constants.resize())
		layerPlay.add(back)
		back.x = join.x
		back.y = join.y - (2 * (MathHelper.inst.pxToScreen(5f) + join.height))
		back.addListener(CL.inst.playBack)
		
		layerPlay.isVisible = false
		
		return layerPlay
	}
	
	fun buildHost(): Table
	{
		layerHost.clear()
		
		layerHost.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerHost.add(background)
		
		var host = Image(skin, "ui_host")
		host.setSize(host.width * Constants.resize(), host.height * Constants.resize())
		layerHost.add(host)
		host.x = (Gdx.graphics.width / 2f) - (host.width / 2f)
		host.y = (Gdx.graphics.height - host.height) - MathHelper.inst.pxToScreen(10f)
		
		var leftMap = Button(skin, "left")
		leftMap.setSize(leftMap.width * Constants.resize(), leftMap.height * Constants.resize())
		layerHost.add(leftMap)
		leftMap.x = (host.x + (host.width / 2f)) - MathHelper.inst.pxToScreen(8f)
		leftMap.y = host.y - MathHelper.inst.pxToScreen(24f)
		leftMap.addListener(CL.inst.hostMapLeft)
		
		var mapName = Image(skin, "ui_host_map_classic")
		mapName.setSize(mapName.width * Constants.resize(), mapName.height * Constants.resize())
		layerHost.add(mapName)
		mapName.x = leftMap.x + leftMap.width + MathHelper.inst.pxToScreen(1f)
		mapName.y = leftMap.y
		
		var rightMap = Button(skin, "right")
		rightMap.setSize(rightMap.width * Constants.resize(), rightMap.height * Constants.resize())
		layerHost.add(rightMap)
		rightMap.x = mapName.x + mapName.width + MathHelper.inst.pxToScreen(1f)
		rightMap.y = mapName.y
		rightMap.addListener(CL.inst.hostMapRight)
		
		var leftPenalty = Button(skin, "left")
		leftPenalty.setSize(leftMap.width, leftMap.height)
		layerHost.add(leftPenalty)
		leftPenalty.x = leftMap.x
		leftPenalty.y = leftMap.y - MathHelper.inst.pxToScreen(22f)
		leftPenalty.addListener(CL.inst.hostPenaltyLeft)
		
		var penalty = Image(skin, Penalty.POINTS_X10.drawable)
		penalty.setSize(penalty.width * Constants.resize(), penalty.height * Constants.resize())
		layerHost.add(penalty)
		penalty.x = leftPenalty.x + leftPenalty.width + MathHelper.inst.pxToScreen(1f)
		penalty.y = leftPenalty.y
		
		var rightPenalty = Button(skin, "right")
		rightPenalty.setSize(rightMap.width, rightMap.height)
		layerHost.add(rightPenalty)
		rightPenalty.x = rightMap.x
		rightPenalty.y = leftPenalty.y
		rightPenalty.addListener(CL.inst.hostPenaltyRight)
		
		var penaltyDescription = Image(skin, Penalty.POINTS_X10.descDrawable)
		penaltyDescription.setSize(penaltyDescription.width * Constants.resize(), penaltyDescription.height * Constants.resize())
		layerHost.add(penaltyDescription)
		penaltyDescription.x = MathHelper.inst.pxToScreen(5f)
		penaltyDescription.y = (penalty.y + penalty.height) - penaltyDescription.height
		
		var start = Button(skin, "start_wide")
		start.setSize(start.width * Constants.resize(), start.height * Constants.resize())
		layerHost.add(start)
		start.x = leftMap.x
		start.y = MathHelper.inst.pxToScreen(5f)
		start.addListener(CL.inst.hostStart)
		
		var back = Button(skin, "back_wide")
		back.setSize(back.width * Constants.resize(), back.height * Constants.resize())
		layerHost.add(back)
		back.x = (start.x + start.width) + MathHelper.inst.pxToScreen(3f)
		back.y = start.y
		back.addListener(CL.inst.hostBack)
		
		layerHost.isVisible = false
		
		return layerHost
	}
	
	fun buildJoin(): Table
	{
		layerJoin.clear()
		
		layerJoin.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerJoin.add(background)
		
		var join = Image(skin, "ui_join")
		join.setSize(join.width * Constants.resize(), join.height * Constants.resize())
		layerJoin.add(join)
		join.x = (Gdx.graphics.width / 2f) - (join.width / 2f)
		join.y = (Gdx.graphics.height - join.height) - MathHelper.inst.pxToScreen(10f)
		
		var up1 = Button(skin, "up")
		layerJoin.add(up1)
		up1.setSize(up1.width * Constants.resize(), up1.height * Constants.resize())
		up1.x = join.x
		up1.y = join.y - MathHelper.inst.pxToScreen(24f)
		up1.addListener(CL.inst.joinUp[0])
		
		var up2 = Button(skin, "up")
		layerJoin.add(up2)
		up2.setSize(up2.width * Constants.resize(), up2.height * Constants.resize())
		up2.x = (join.x + (join.width / 2f)) - (up2.width / 2f)
		up2.y = up1.y
		up2.addListener(CL.inst.joinUp[1])
		
		var up3 = Button(skin, "up")
		layerJoin.add(up3)
		up3.setSize(up3.width * Constants.resize(), up3.height * Constants.resize())
		up3.x = (join.x + join.width) - up3.width
		up3.y = up2.y
		up3.addListener(CL.inst.joinUp[2])
		
		var down1 = Button(skin, "down")
		layerJoin.add(down1)
		down1.setSize(up1.width, up1.height)
		down1.x = up1.x
		down1.y = up1.y - MathHelper.inst.pxToScreen(24f)
		down1.addListener(CL.inst.joinDown[0])
		
		var down2 = Button(skin, "down")
		layerJoin.add(down2)
		down2.setSize(up2.width, up2.height)
		down2.x = up2.x
		down2.y = up2.y - MathHelper.inst.pxToScreen(24f)
		down2.addListener(CL.inst.joinDown[1])
		
		var down3 = Button(skin, "down")
		layerJoin.add(down3)
		down3.setSize(up3.width, up3.height)
		down3.x = up3.x
		down3.y = up3.y - MathHelper.inst.pxToScreen(24f)
		down3.addListener(CL.inst.joinDown[2])
		
		int1 = Image(skin, "ui_int_1")
		layerJoin.add(int1)
		int1!!.setSize(int1!!.width * Constants.resize(), int1!!.height * Constants.resize())
		int1!!.x = up1.x
		int1!!.y = up1.y - MathHelper.inst.pxToScreen(9f)
		
		int2 = Image(skin, "ui_int_2")
		layerJoin.add(int2)
		int2!!.setSize(int1!!.width, int1!!.height)
		int2!!.x = up2.x
		int2!!.y = int1!!.y
		
		int3 = Image(skin, "ui_int_3")
		layerJoin.add(int3)
		int3!!.setSize(int2!!.width, int2!!.height)
		int3!!.x = up3.x
		int3!!.y = int2!!.y
		
		var btnJoin = Button(skin, "join_small")
		layerJoin.add(btnJoin)
		btnJoin.setSize(btnJoin.width * Constants.resize(), btnJoin.height * Constants.resize())
		btnJoin.x = join.x - ((((btnJoin.width * 2f) + MathHelper.inst.pxToScreen(2f)) - join.width) / 2f)
		//btnJoin.x = join.x - MathHelper.inst.pxToScreen(3f)
		btnJoin.y = MathHelper.inst.pxToScreen(5f)
		btnJoin.addListener(CL.inst.joinJoin)
		
		var back = Button(skin, "back_small")
		layerJoin.add(back)
		back.setSize(back.width * Constants.resize(), back.height * Constants.resize())
		back.x = (btnJoin.x + btnJoin.width) + MathHelper.inst.pxToScreen(3f)
		back.y = btnJoin.y
		back.addListener(CL.inst.joinBack)
		
		connecting = Label("connecting...", skin, "default")
		layerJoin.add(connecting)
		connecting!!.setFontScale(Constants.resize() / 2f)
		connecting!!.y = btnJoin.y + btnJoin.height + MathHelper.inst.pxToScreen(5f)
		
		var glyph = GlyphLayout()
		glyph.setText(connecting!!.style.font, "connecting")
		
		connecting!!.x = (btnJoin.x + ((btnJoin.width * 2f + MathHelper.inst.pxToScreen(3f)) / 2f)) - MathHelper.inst.pxToScreen(glyph.width / 4f)
		
		layerJoin.isVisible = false
		
		return layerJoin
	}
	
	fun buildLobby(): Table
	{
		layerLobby.clear()
		
		layerLobby.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerLobby.add(background)
		
		var lobby = Image(skin, "ui_lobby")
		lobby.setSize(lobby.width * Constants.resize(), lobby.height * Constants.resize())
		layerLobby.add(lobby)
		lobby.x = (Gdx.graphics.width / 2f) - (lobby.width / 2f)
		lobby.y = (Gdx.graphics.height - lobby.height) - MathHelper.inst.pxToScreen(10f)
		
		var waiting = Image(skin, "ui_lobby_waiting")
		waiting.setSize(waiting.width * Constants.resize(), waiting.height * Constants.resize())
		layerLobby.add(waiting)
		waiting.y = lobby.y - MathHelper.inst.pxToScreen(24f)
		
		var configWidth = waiting.width * 2f + MathHelper.inst.pxToScreen(12f)
		
		waiting.x = (lobby.x + lobby.width / 2f) - (configWidth / 2f)
		
		waitingText = Label("120", skin, "border")
		waitingText!!.setFontScale(Constants.resize())
		waitingText!!.style.font.setUseIntegerPositions(false)
		layerLobby.add(waitingText)
		waitingText!!.x = waiting.x + MathHelper.inst.pxToScreen(6f)
		
		var textTranslate = 0f
		if(Constants.resize() != MathHelper.inst.biggestPrime(Constants.resize()) * 2f)
			textTranslate = MathHelper.inst.biggestPrime(Constants.resize())
		
		//if(textTranslate > 0)
		//	textTranslate = 0.5f
		
		waitingText!!.y = waiting.y + MathHelper.inst.pxToScreen(6f) + textTranslate
		
		println("Biggest Prime: ${MathHelper.inst.biggestPrime(Constants.resize())}")
		
		var id = Image(skin, "ui_lobby_id")
		id.setSize(id.width * Constants.resize(), id.height * Constants.resize())
		layerLobby.add(id)
		id.x = waiting.x + waiting.width + MathHelper.inst.pxToScreen(12f)
		id.y = waiting.y
		
		layerLobby.isVisible = false
		
		var map = Image(skin, "ui_host_map_classic")
		map.setSize(waiting.width, waiting.height)
		layerLobby.add(map)
		map.x = waiting.x
		map.y = waiting.y - MathHelper.inst.pxToScreen(24f)
		
		var penalty = Image(skin, "ui_host_map_penalty_points")
		penalty.setSize(id.width, id.height)
		layerLobby.add(penalty)
		penalty.x = id.x
		penalty.y = map.y
		
		var leave = Button(skin, "back")
		leave.setSize(leave.width * Constants.resize(), leave.height * Constants.resize())
		layerLobby.add(leave)
		leave.x = (lobby.x + lobby.width) - leave.width
		leave.y = MathHelper.inst.pxToScreen(5f)
		leave.addListener(CL.inst.lobbyBack)
		
		return layerLobby
	}
	
	fun setWaitingText(text: String) { waitingText!!.setText("$text") }
	
	fun buildGame(): Table
	{
		layerGame.clear()
		
		layerGame.setLayoutEnabled(false)
		
		var strokes = Image(skin, "ui_strokes_counter")
		strokes.setSize(strokes.width * Constants.resize(), strokes.height * Constants.resize())
		layerGame.add(strokes)
		strokes.x = MathHelper.inst.pxToScreen(5f)
		strokes.y = MathHelper.inst.pxToScreen(5f)
		
		shoot = Button(skin, "shoot")
		shoot!!.setSize(shoot!!.width * Constants.resize(), shoot!!.height * Constants.resize())
		layerGame.add(shoot)
		shoot!!.x = (Gdx.graphics.width / 2f) - (shoot!!.width / 2f)
		shoot!!.y = strokes.y
		shoot!!.addListener(CL.inst.gameShoot)
		
		point = Button(skin, "point")
		point!!.setSize(point!!.width * Constants.resize(), point!!.height * Constants.resize())
		layerGame.add(point)
		point!!.x = shoot!!.x
		point!!.y = strokes.y
		point!!.addListener(CL.inst.gamePoint)
		
		powerbar = Image(skin, "ui_shoot_strength_12")
		powerbar!!.setSize(powerbar!!.width * Constants.resize(), powerbar!!.height * Constants.resize())
		layerGame.add(powerbar)
		powerbar!!.x = point!!.x
		powerbar!!.y = point!!.y + point!!.height + MathHelper.inst.pxToScreen(1f)
		
		arrow = Image(skin, "ui_pointer")
		arrow!!.setSize(arrow!!.width * Constants.resize(), arrow!!.height * Constants.resize())
		layerGame.add(arrow)
		arrow!!.originX = 0f
		arrow!!.originY = arrow!!.height / 2f
		arrow!!.rotateBy(90f)
		arrow!!.x = Gdx.graphics.width / 2f
		arrow!!.y = Gdx.graphics.height / 2f - MathHelper.inst.pxToScreen(3.5f)
		
		layerGame.isVisible = false
		
		return layerGame
	}
	
	fun buildColorPicker(): Table
	{
		layerColorPicker.clear()
		
		layerColorPicker.setLayoutEnabled(false)
		
		var background = Image(skin, "px_background")
		background.setSize(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		layerColorPicker.add(background)
		
		var youreNew = Image(skin, "ui_youre_new")
		youreNew.setSize(youreNew.width * Constants.resize(), youreNew.height * Constants.resize())
		layerColorPicker.add(youreNew)
		youreNew.x = (Gdx.graphics.width / 2f) - (youreNew.width / 2f)
		youreNew.y = (Gdx.graphics.height - youreNew.height) - MathHelper.inst.pxToScreen(10f)
		
		var leftR = Button(skin, "left")
		leftR.setSize(leftR.width * Constants.resize(), leftR.height * Constants.resize())
		layerColorPicker.add(leftR)
		leftR.x = (Gdx.graphics.width / 2f) - MathHelper.inst.pxToScreen(103f / 2f)
		leftR.y = youreNew.y - MathHelper.inst.pxToScreen(24f)
		leftR.addListener(CL.inst.colorRLeft)
		
		var colorR = Image(skin, "ui_color_r")
		colorR.setSize(colorR.width * Constants.resize(), colorR.height * Constants.resize())
		layerColorPicker.add(colorR)
		colorR.x = leftR.x + leftR.width + MathHelper.inst.pxToScreen(1f)
		colorR.y = leftR.y
		
		pickerMaxLeft = colorR.x + MathHelper.inst.pxToScreen(2f)
		pickerMaxRight = pickerMaxLeft + MathHelper.inst.pxToScreen(7f * 4f)
		
		pickerR = Image(skin, "ui_color_picker")
		pickerR!!.setSize(pickerR!!.width * Constants.resize(), pickerR!!.height * Constants.resize())
		layerColorPicker.add(pickerR)
		pickerR!!.x = pickerMaxRight
		pickerR!!.y = colorR.y
		
		var rightR = Button(skin, "right")
		rightR.setSize(rightR.width * Constants.resize(), rightR.height * Constants.resize())
		layerColorPicker.add(rightR)
		rightR.x = colorR.x + colorR.width + MathHelper.inst.pxToScreen(1f)
		rightR.y = colorR.y
		rightR.addListener(CL.inst.colorRRight)
		
		var colorDemo = Image(skin, "ui_color_demo")
		colorDemo.setSize(colorDemo.width * Constants.resize(), colorDemo.height * Constants.resize())
		layerColorPicker.add(colorDemo)
		colorDemo.x = rightR.x + rightR.width + MathHelper.inst.pxToScreen(10f)
		colorDemo.y = rightR.y - MathHelper.inst.pxToScreen(1f)
		
		ballColor = Image(skin, "px_white")
		ballColor!!.setSize(MathHelper.inst.pxToScreen(2f), MathHelper.inst.pxToScreen(2f))
		layerColorPicker.add(ballColor)
		ballColor!!.x = colorDemo.x + MathHelper.inst.pxToScreen(13f)
		ballColor!!.y = colorDemo.y + MathHelper.inst.pxToScreen(11f)
		
		var leftG = Button(skin, "left")
		leftG.setSize(leftG.width * Constants.resize(), leftG.height * Constants.resize())
		layerColorPicker.add(leftG)
		leftG.x = leftR.x
		leftG.y = leftR.y - MathHelper.inst.pxToScreen(24f)
		leftG.addListener(CL.inst.colorGLeft)
		
		var colorG = Image(skin, "ui_color_g")
		colorG.setSize(colorG.width * Constants.resize(), colorG.height * Constants.resize())
		layerColorPicker.add(colorG)
		colorG.x = colorR.x
		colorG.y = leftG.y
		
		pickerG = Image(skin, "ui_color_picker")
		pickerG!!.setSize(pickerG!!.width * Constants.resize(), pickerG!!.height * Constants.resize())
		layerColorPicker.add(pickerG)
		pickerG!!.x = pickerMaxRight
		pickerG!!.y = colorG.y
		
		var rightG = Button(skin, "right")
		rightG.setSize(rightG.width * Constants.resize(), rightG.height * Constants.resize())
		layerColorPicker.add(rightG)
		rightG.x = rightR.x
		rightG.y = colorG.y
		rightG.addListener(CL.inst.colorGRight)
		
		var leftB = Button(skin, "left")
		leftB.setSize(leftB.width * Constants.resize(), leftB.height * Constants.resize())
		layerColorPicker.add(leftB)
		leftB.x = leftG.x
		leftB.y = leftG.y - MathHelper.inst.pxToScreen(24f)
		leftB.addListener(CL.inst.colorBLeft)
		
		var colorB = Image(skin, "ui_color_b")
		colorB.setSize(colorB.width * Constants.resize(), colorB.height * Constants.resize())
		layerColorPicker.add(colorB)
		colorB.x = colorG.x
		colorB.y = leftB.y
		
		pickerB = Image(skin, "ui_color_picker")
		pickerB!!.setSize(pickerB!!.width * Constants.resize(), pickerB!!.height * Constants.resize())
		layerColorPicker.add(pickerB)
		pickerB!!.x = pickerMaxRight
		pickerB!!.y = colorB.y
		
		var rightB = Button(skin, "right")
		rightB.setSize(rightB.width * Constants.resize(), rightB.height * Constants.resize())
		layerColorPicker.add(rightB)
		rightB.x = rightG.x
		rightB.y = colorB.y
		rightB.addListener(CL.inst.colorBRight)
		
		var play = Button(skin, "play")
		play.setSize(play.width * Constants.resize(), play.height * Constants.resize())
		layerColorPicker.add(play)
		play.x = (Gdx.graphics.width / 2f) - (play.width / 2f)
		play.y = MathHelper.inst.pxToScreen(5f)
		play.addListener(CL.inst.colorPlay)
		
		return layerColorPicker
	}
	
	/**
	 * direction -> 0 = left, 1 = right
	 * color -> r = red, g = green, b = blue
	 */
	fun colorPicker(direction: Int, color: String)
	{
		if(color == "r")
		{
			when(direction)
			{
				0 -> if(pickerR!!.x > pickerMaxLeft) pickerR!!.moveBy(MathHelper.inst.pxToScreen(-7f), 0f) else pickerR!!.x = pickerMaxRight
				1 -> if(pickerR!!.x < pickerMaxRight) pickerR!!.moveBy(MathHelper.inst.pxToScreen(7f), 0f) else pickerR!!.x = pickerMaxLeft
			}
		}
		else if(color == "g")
		{
			when(direction)
			{
				0 -> if(pickerG!!.x > pickerMaxLeft) pickerG!!.moveBy(MathHelper.inst.pxToScreen(-7f), 0f) else pickerG!!.x = pickerMaxRight
				1 -> if(pickerG!!.x < pickerMaxRight) pickerG!!.moveBy(MathHelper.inst.pxToScreen(7f), 0f) else pickerG!!.x = pickerMaxLeft
			}
		}
		else if(color == "b")
		{
			when(direction)
			{
				0 -> if(pickerB!!.x > pickerMaxLeft) pickerB!!.moveBy(MathHelper.inst.pxToScreen(-7f), 0f) else pickerB!!.x = pickerMaxRight
				1 -> if(pickerB!!.x < pickerMaxRight) pickerB!!.moveBy(MathHelper.inst.pxToScreen(7f), 0f) else pickerB!!.x = pickerMaxLeft
			}
		}
		
		colorRVal = ((pickerR!!.x - pickerMaxLeft) / MathHelper.inst.pxToScreen(7f)) / 5f + 0.2f
		colorGVal = ((pickerG!!.x - pickerMaxLeft) / MathHelper.inst.pxToScreen(7f)) / 5f + 0.2f
		colorBVal = ((pickerB!!.x - pickerMaxLeft) / MathHelper.inst.pxToScreen(7f)) / 5f + 0.2f
		
		ballColor!!.setColor(colorRVal, colorGVal, colorBVal, ballColor!!.color.a)
	}
	
	fun update()
	{
		// Arrow
		if(!donePointing)
		{
			if(arrow!!.rotation == 170f)
				arrowClockwise = true
			else if(arrow!!.rotation == 10f)
				arrowClockwise = false
			
			when(arrowClockwise)
			{
				true -> arrow!!.rotateBy(-2f)
				false -> arrow!!.rotateBy(2f)
			}
		}
		
		/*if(currentMenu == Menu.GAME)
		{
			NetClient.updatePosition()
			//if(!NetClient.connect())
			//	Scene2DHelper.showMenu(Menu.PLAY)
		}*/
	}
	
	fun pointing(done: Boolean)
	{
		if(done)
		{
			donePointing = true
			point!!.isVisible = false
			
			GolfballHandler.powerbarCharge = 0
			GolfballHandler.canCharge = true
		}
		else
		{
			donePointing = false
			point!!.isVisible = true
			shoot!!.setColor(shoot!!.color.r, shoot!!.color.g, shoot!!.color.b, 1f)
			arrow!!.setColor(arrow!!.color.r, arrow!!.color.g, arrow!!.color.b, 1f)
			powerbar!!.setDrawable(skin.get("ui_shoot_strength_0", Drawable::class.java))
		}
	}
	
	fun shot()
	{
		if(!NetClient.connected())
			Scene2DHelper.showMenu(Menu.PLAY)
		
		if(!ShootTask.isScheduled)
		{
			GolfballHandler.canCharge = false
			shoot!!.setColor(shoot!!.color.r, shoot!!.color.g, shoot!!.color.b, 0.5f)
			arrow!!.setColor(arrow!!.color.r, arrow!!.color.g, arrow!!.color.b, 0.5f)
			Timer.schedule(ShootTask, 3f)
			//GolfballHandler.shootBall(arrow!!.rotation)
			GolfballHandler.shootBall(arrow!!.rotation)
		}
	}
	
	fun setChargeBarPower(powerbarCharge: Int)
	{
		powerbar!!.setDrawable(skin.get("ui_shoot_strength_$powerbarCharge", Drawable::class.java))
	}
	
	fun clickLobbyID(position: Int, clickedUp: Boolean)
	{
		if(clickedUp)
		{
			when(position)
			{
				1 -> if(counter1 + 1 >= 3) counter1 = 0 else counter1++
				2 -> if(counter2 + 1 >= 10) counter2 = 0 else if((counter1 * 100 + counter2 * 10 + counter3) != 254) counter2++ else counter2 = 0
				3 -> if(counter3 + 1 >= 10) counter3 = 0 else if((counter1 * 100 + counter2 * 10 + counter3) != 254) counter3++ else counter3 = 0
			}
		}
		else
		{
			when(position)
			{
				1 -> if(counter1 - 1 < 0) counter1 = 2 else counter1--
				2 -> if(counter2 - 1 < 0) counter2 = 9 else counter2--
				3 -> if(counter3 - 1 < 0) counter3 = 9 else counter3--
			}
		}
		
		var ip = counter1 * 100 + counter2 * 10 + counter3
		
		if(ip > 254)
		{
			counter1 = 2
			counter2 = 5
			counter3 = 4
		}
		
		/*if(counter1 == 0 && counter2 == 0)
			counter3 = 1*/
		
		int1!!.setDrawable(skin.get("ui_int_$counter1", Drawable::class.java))
		int2!!.setDrawable(skin.get("ui_int_$counter2", Drawable::class.java))
		int3!!.setDrawable(skin.get("ui_int_$counter3", Drawable::class.java))
		
		lobbyId = ""
		
		if(counter1 > 0)
		{
			lobbyId = "$counter1$counter2$counter3"
		}
		else
		{
			if(counter2 > 0)
			{
				lobbyId = "$counter2$counter3"
			}
			else
			{
				lobbyId = "$counter3"
			}
		}
		
	}
	
	fun showMenu(menu: Menu)
	{
		when(currentMenu)
		{
			Menu.COLOR_PICKER -> layerColorPicker.isVisible = false
			Menu.MAIN_MENU -> layerMainMenu.isVisible = false
			Menu.PLAY -> layerPlay.isVisible = false
			Menu.HOST -> layerHost.isVisible = false
			Menu.JOIN -> layerJoin.isVisible = false
			Menu.LOBBY -> layerLobby.isVisible = false
			Menu.GAME -> layerGame.isVisible = false
		}
		
		currentMenu = menu
		
		if(currentMenu == Menu.JOIN)
			connecting!!.setText("")
		
		when(currentMenu)
		{
			Menu.COLOR_PICKER -> layerColorPicker.isVisible = Constants.showUi
			Menu.MAIN_MENU -> layerMainMenu.isVisible = Constants.showUi
			Menu.PLAY -> layerPlay.isVisible = Constants.showUi
			Menu.HOST -> layerHost.isVisible = Constants.showUi
			Menu.JOIN -> layerJoin.isVisible = Constants.showUi
			Menu.LOBBY -> layerLobby.isVisible = Constants.showUi
			Menu.GAME -> layerGame.isVisible = Constants.showUi
		}
	}
	
	fun setConnectingText(text: String)
	{
		connecting!!.setText(text)
		
		var glyphText = text
		if(glyphText.endsWith("..."))
			glyphText.replace("...", "")
		
		var glyph = GlyphLayout()
		glyph.setText(connecting!!.style.font, glyphText)
		connecting!!.x = (Gdx.graphics.width / 2f) - MathHelper.inst.pxToScreen(glyph.width / 4f)
	}
	
	fun buildDebug(): Table
	{
		var layer = Table()
		
		layer.top().left().padLeft(MathHelper.inst.pxToScreen(1f))
		
		var disclaimer = Label("early access build, do not distribute", skin, "default")
		disclaimer.setFontScale(Constants.resize() / 2f)
		layer.add(disclaimer)
		
		/*var host = Button(skin, "host")
		layer.add(host).size(host.width * Constants.resize(), host.height * Constants.resize())
		host.addListener(ChangeListeners.inst.host)*/
		
		/*var join = Button(skin, "join")
		layer.add(join).size(join.width * Constants.resize(), join.height * Constants.resize())
		join.addListener(ChangeListeners.inst.join)*/
		
		return layer
	}
}