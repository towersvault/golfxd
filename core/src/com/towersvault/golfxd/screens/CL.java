package com.towersvault.golfxd.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Timer;
import com.towersvault.golfxd.game.GolfballHandler;
import com.towersvault.golfxd.game.ShootTask;
import com.towersvault.golfxd.net.host.GameHandler;
import com.towersvault.golfxd.net.client.NetClient;
import com.towersvault.golfxd.net.host.NetServer;
import com.towersvault.golfxd.util.Log;

public class CL
{
	public static final CL inst = new CL();
	
	public ChangeListener colorRLeft;
	public ChangeListener colorRRight;
	public ChangeListener colorGLeft;
	public ChangeListener colorGRight;
	public ChangeListener colorBLeft;
	public ChangeListener colorBRight;
	public ChangeListener colorPlay;
	
	public ChangeListener menuPlay;
	public ChangeListener menuOptions;
	public ChangeListener menuExit;
	
	public ChangeListener playHost;
	public ChangeListener playJoin;
	public ChangeListener playBack;
	
	public ChangeListener hostMapLeft;
	public ChangeListener hostMapRight;
	public ChangeListener hostPenaltyLeft;
	public ChangeListener hostPenaltyRight;
	public ChangeListener hostStart;
	public ChangeListener hostBack;
	
	public ChangeListener joinJoin;
	public ChangeListener joinBack;
	public ChangeListener[] joinUp;
	public ChangeListener[] joinDown;
	
	public ChangeListener lobbyBack;
	
	public ChangeListener gamePoint;
	public ChangeListener gameShoot;
	
	public void init()
	{
		joinUp = new ChangeListener[3];
		joinDown = new ChangeListener[3];
		
		colorRLeft = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(0, "r");
			}
		};
		
		colorRRight = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(1, "r");
			}
		};
		
		colorGLeft = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(0, "g");
			}
		};
		
		colorGRight = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(1, "g");
			}
		};
		
		colorBLeft = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(0, "b");
			}
		};
		
		colorBRight = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.colorPicker(1, "b");
			}
		};
		
		colorPlay = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				GolfballHandler.INSTANCE.getYourGolfball().setBallColor(Scene2DHelper.INSTANCE.getColorRVal(),
						Scene2DHelper.INSTANCE.getColorGVal(),
						Scene2DHelper.INSTANCE.getColorBVal());
				
				Scene2DHelper.INSTANCE.showMenu(Menu.MAIN_MENU);
			}
		};
		
		menuPlay = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.PLAY);
			}
		};
		
		menuOptions = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.COLOR_PICKER);
			}
		};
		
		menuExit = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Gdx.app.exit();
			}
		};
		
		playHost = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.HOST);
			}
		};
		
		playJoin = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.JOIN);
			}
		};
		
		playBack = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.MAIN_MENU);
			}
		};
		
		hostMapLeft = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Log.post("No other maps have been added yet.");
			}
		};
		
		hostMapRight = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Log.post("No other maps have been added yet.");
			}
		};
		
		hostPenaltyLeft = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Log.post("No other penalties can be set yet.");
			}
		};
		
		hostPenaltyRight = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Log.post("No other penalties can be set yet.");
			}
		};
		
		hostStart = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				System.out.println("------------------------------");
				NetServer.INSTANCE.open();
				Scene2DHelper.INSTANCE.showMenu(Menu.LOBBY);
			}
		};
		
		hostBack = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.PLAY);
			}
		};
		
		joinJoin = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				NetClient.INSTANCE.connect();
			}
		};
		
		joinBack = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.showMenu(Menu.PLAY);
			}
		};
		
		joinUp[0] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(1, true);
			}
		};
		
		joinUp[1] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(2, true);
			}
		};
		
		joinUp[2] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(3, true);
			}
		};
		
		joinDown[0] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(1, false);
			}
		};
		
		joinDown[1] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(2, false);
			}
		};
		
		joinDown[2] = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.clickLobbyID(3, false);
			}
		};
		
		lobbyBack = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				if(NetServer.INSTANCE.isServerOpen())
				{
					NetServer.INSTANCE.close();
					GameHandler.INSTANCE.hostCloseSession();
				}
				
				Scene2DHelper.INSTANCE.showMenu(Menu.PLAY);
				
				System.out.println("------------------------------");
			}
		};
		
		gamePoint = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				Scene2DHelper.INSTANCE.pointing(true);
			}
		};
		
		
		gameShoot = new ChangeListener()
		{
			@Override
			public void changed(ChangeEvent event, Actor actor)
			{
				//Log.post("Add code for actually hitting the ball.");
				Scene2DHelper.INSTANCE.shot();
			}
		};
	}
	
	public static void getBytes(String text)
	{
		System.out.println("B: " + text.getBytes().length);
	}
}
