package com.towersvault.golfxd.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Array
import com.towersvault.golfxd.net.NetMessage
import com.towersvault.golfxd.util.Constants
import com.towersvault.golfxd.util.Log
import com.towersvault.golfxd.util.MathHelper

object Scene2DChat
{
	private var textLabels = Array<Label>()
	private var layer = Table()
	
	fun init()
	{
		textLabels.clear()
	}
	
	fun build(): Table
	{
		layer.clear()
		
		layer.setLayoutEnabled(false)
		
		for(i in 0..10)
		{
			var line = Label("test $i", Scene2DHelper.skin, "border")
			line.setFontScale(Constants.resize() / 2f)
			layer.add(line)
			line.x = Gdx.graphics.width / 2f
			line.y = Gdx.graphics.height / 2f - MathHelper.inst.pxToScreen(i * 8f)
		}
		
		return layer
	}
	
	fun parseMessage(netMessage: NetMessage)
	{
		Log.post("${netMessage.username} > ${netMessage.message}")
	}
}