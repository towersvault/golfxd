package com.towersvault.golfxd.screens

enum class Menu
{
	COLOR_PICKER,
	MAIN_MENU,
	PLAY,
	HOST,
	JOIN,
	LOBBY,
	GAME,
	GAME_SCORES
}